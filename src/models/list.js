mongoose = require('mongoose');
const Joi = require('joi');

const listSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        maxlength: 255
    },
    type: {
        type: String,
        trim: true,
        minlength: 1,
        maxlength: 30
    }, 
    location: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        maxlength: 50
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    restaurants: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'restaurants'
    }]
});

const List = mongoose.model('lists', listSchema);

function validateList(list) {
    const schema = {
        title: Joi.string().min(1).max(255).required(),
        type: Joi.string().min(1).max(30),
        location: Joi.string().min(1).max(50).required(),
        user: Joi.objectId().required(),
        restaurants: Joi.objectId()
    };

    return Joi.validate(list, schema);
}

exports.List = List;
exports.validate = validateList;