const request = require('supertest');
const { List } = require('../../models/list');
const { User } = require('../../models/user');
const mongoose = require('mongoose');

let server;

xdescribe('/api/lists', () => {
    beforeEach(() => {
        server = require('../../../index');
    });

    afterEach(async () => {
        await List.deleteMany({});
        await User.deleteMany({});
        await server.close();
    });

    afterAll(() => {
        mongoose.connection.close();
    });

    describe('GET /me', () => {
        let token;
        let user;
        let list;
        let list2;

        const exec = async () => {
            return await request(server)
                .get('/api/lists/me')
                .set('x-auth-token', token)
                .send(user);
        };

        beforeEach(async () => {
            userId = mongoose.Types.ObjectId();
            listId = mongoose.Types.ObjectId();
            listId2 = mongoose.Types.ObjectId();
            token = '12345';

            user = new User({
                _id: userId,
                name: 'name1',
                email: 'email1',
                firebaseId: '12345',
            });
            await user.save();

            list = new List({
                _id: listId,
                title: 'title1',
                location: 'location1',
                user: userId,
            });
            await list.save();

            list2 = new List({
                _id: listId2,
                title: 'title2',
                location: 'location2',
                user: userId,
            });
            await list2.save();
        });

        it('should return 401 if user is not logged in', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return all lists', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some((l) => l.title === 'title1')).toBeTruthy();
            expect(res.body.some((l) => l.title === 'title2')).toBeTruthy();
        });
    });

    describe('POST /', () => {
        let token;
        let user;
        let userId;
        let list;

        const exec = () => {
            return request(server)
                .post('/api/lists/')
                .set('x-auth-token', token)
                .send(list);
        };

        beforeEach(async () => {
            userId = mongoose.Types.ObjectId();
            token = '12345';

            user = new User({
                _id: userId,
                name: 'name1',
                email: 'email1',
                firebaseId: '12345',
            });
            await user.save();

            list = {
                title: 'title1',
                location: 'location1',
                user: userId,
            };
        });
        it('should add a new list to DB', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('title', list.title);
        });
    });
});
