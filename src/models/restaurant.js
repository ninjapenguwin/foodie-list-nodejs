const mongoose = require('mongoose');
const Joi = require('joi');

const restaurantSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 255
    },
    location: {
        type: String,
        minlength: 3,
        maxlength: 50
    },
    favorites: {
        type: String,
        minlength: 3,
        maxlength: 255
    },
    rating: {
        type: Number,
        min: 1,
        max: 100
    },
    list: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'lists',
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    }
});

const Restaurant = mongoose.model('restaurants', restaurantSchema);

function validateRestaurant(restaurant) {
    const schema = {
        name: Joi.string().max(255).min(3).required(),
        location: Joi.string().max(50).min(3),
        favorites: Joi.string().max(255).min(3),
        rating: Joi.number().integer().min(1).max(100),
        list: Joi.objectId().required(),
        user: Joi.objectId()
    }

    return Joi.validate(restaurant, schema);
};

exports.Restaurant = Restaurant;
exports.validate = validateRestaurant;