const { List, validate } = require('../models/list');
const { Restaurant } = require('../models/restaurant');
const auth = require('../middleware/auth');
const validateObjectId = require('../middleware/validateObjectId');
const express = require('express');
const router = express.Router();

// Get user's lists
router.get('/me', auth, async (req, res) => {
    const lists = await List.find({ user: req.user._id });
    res.send(lists)
});

// Add new list
router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let list = new List({
        title: req.body.title,
        type: req.body.type,
        location: req.body.location,
        user: req.user._id,
        restaurants: req.body.restaurants
    });

    list = await list.save();

    res.send(list);
});

// Delete list
router.delete('/:id', auth, async (req, res) => {
    const list = await List.findById(req.params.id);
    if (!list) return res.status(404).send('List with given ID does not exist.');

    if (!list.user.toString() === req.user._id) {
        return res.status(403).send('Access denied.');
    }

    await Restaurant.deleteMany({ list: req.params.id });
    await List.findOneAndDelete(req.params.id);

    res.send(list);
});

// Update list
router.put('/:id', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let list = await List.findById(req.params.id);
    if (!list) return res.status(404).send('List with given ID does not exist.');

    if (!list.user.toString() === req.user._id) {
        return res.status(403).send('Access denied.');
    }

    list = await List.findByIdAndUpdate(req.params.id, 
        {$set: req.body},
        { new: true });

    res.send(list);
});

module.exports = router;