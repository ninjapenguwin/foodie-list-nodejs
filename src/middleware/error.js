const winston = require('winston');

module.exports = function(err, req, res, next){
    
    winston.error(err.message, err);

    // error
    // warning
    // info
    // verbose
    // debug
    // silly

    process.on('uncaughtException', (ex) => {
        console.log('UNCAUGHT EXCEPTION');
        winston.error(ex.message, ex);
    });

    res.status(500).send('Something failed.');
}