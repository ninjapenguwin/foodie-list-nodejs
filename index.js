const express = require('express');
const app = express();
const winston = require('winston');

require('./src/startup/logging')();
require('./src/startup/routes')(app);
require('./src/startup/db')();
require('./src/startup/config')();
require('./src/startup/validation')();

const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
    winston.info(`Listening on port ${port}...`);
});

module.exports = server;
