const { User } = require('../../../models/user');
const request = require('supertest');

describe('auth middleware', () => {
    let server;

    beforeEach(() => {
        server = require('../../../../index');
    });
    afterEach(async () => {
        await User.deleteMany({});
        await server.close();
    });

    afterAll(() => {
        mongoose.connection.close();
    });

    let token;
    let user;

    const exec = () => {
        return request(server).get('/api/users/me').set('x-auth-token', token);
    };

    beforeEach(async () => {
        user = new User({
            name: 'name1',
            email: 'email1',
            password: 'password1',
        });
        await user.save();

        token = user.generateAuthToken();
    });

    it('should return 401 if no token is provided', async () => {
        token = '';

        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('should return 401 if token is invalid', async () => {
        token = 'a';

        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('should return 404 if user with given ID does not exist', async () => {
        await User.deleteMany({});

        const res = await exec();

        expect(res.status).toBe(404);
    });

    it('should return 200 if token is valid', async () => {
        const res = await exec();

        expect(res.status).toBe(200);
    });
});
