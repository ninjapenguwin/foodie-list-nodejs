const mongoose = require('mongoose');
const config = require('config');
const winston = require('winston');

module.exports = function() {
    const db = config.get('db');
    mongoose
        .connect(db, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false,
            useUnifiedTopology: true,
        })
        .then(() => winston.info(`Connected to ${db}...`))
        .catch(error => {
            winston.info(error);
        });
};
