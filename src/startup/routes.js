const express = require('express');
const auth = require('../routes/auth');
const users = require('../routes/users');
const restaurants = require('../routes/restaurants');
const lists = require('../routes/lists');
const error = require('../middleware/error');

module.exports = function(app) {
  app.use(express.json());
  app.use('/api/auth', auth);
  app.use('/api/users', users);
  app.use('/api/restaurants', restaurants);
  app.use('/api/lists', lists);
  app.use(error);
};
