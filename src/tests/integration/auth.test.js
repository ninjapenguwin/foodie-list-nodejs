const { User } = require('../../models/user');
const request = require('supertest');

describe('/api/auth', () => {
    let server;

    beforeEach(() => {
        server = require('../../../index');
    });

    afterEach(async () => {
        await User.deleteMany({});
        server.close();
    });

    afterAll(() => {
        mongoose.connection.close();
    });

    describe('POST /', () => {
        let user;
        let email;
        let password;

        const exec = () => {
            return request(server).post('/api/auth').send({
                email,
                password,
            });
        };

        beforeEach(async () => {
            user = new User({
                name: 'name1',
                email: 'email1@gmail.com',
                password: 'password1',
            });
            password = user.password;
            user.password = await user.hashPassword(user.password);

            await user.save();

            email = user.email;
            token = user.generateAuthToken();
        });

        it('should return 400 if email is not provided', async () => {
            email = '';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 400 if password is not provided', async () => {
            password = '';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 401 if email or password is invalid', async () => {
            password = 'password2';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 200 if credentials are valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return auth: true if credentials are valid', async () => {
            const res = await exec();

            expect(res.body).toHaveProperty('auth', true);
        });
    });
});
