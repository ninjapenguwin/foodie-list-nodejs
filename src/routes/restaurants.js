const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth')
const { Restaurant, validate } = require('../models/restaurant');
const { List } = require('../models/list');

// Add Restaurant
router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const list = await List.findById(req.body.list);
    if (!list) return res.status(404).send('List with given ID does not exist.');

    if (list.user.toString() !== req.user._id) {
        return res.status(403).send('Access denied.');
    }

    let newRestaurant = new Restaurant({
        name: req.body.name,
        location: req.body.location,
        favorites: req.body.favorites,
        rating: req.body.rating,
        list: req.body.list,
        user: req.user._id
    });

    restaurant = await newRestaurant.save();
    res.send(restaurant);
});

// Get Restaurant
router.get('/:id', auth, async (req, res) => {
    const restaurant = await Restaurant.findById(req.params.id);
    if (!restaurant) return res.status(404).send('Restaurant with given ID does not exist.');

    if (restaurant.user.toString() !== req.user._id) {
        return res.status(403).send('Access denied.');
    }

    res.send(restaurant);
});

// Delete Restaurant
router.delete('/:id', auth, async (req, res) => {
    const restaurant = await Restaurant.findById(req.params.id);
    if (!restaurant) return res.status(404).send('Restaurant with given ID does not exist.');

    if (restaurant.user.toString() !== req.user._id) {
        return res.status(403).send('Access denied.');
    }

    await Restaurant.findOneAndDelete(req.params.id);

    res.send(restaurant);
});

// Update Restaurant
router.put('/:id', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let restaurant = await Restaurant.findById(req.params.id);
    if (!restaurant) return res.status(404).send('Restaurant with given ID does not exist.');

    if (restaurant.user.toString() !== req.user._id) {
        return res.status(403).send('Access denied.');
    }

    restaurant = await Restaurant.findByIdAndUpdate(req.params.id, 
        {$set: req.body},
        { new: true });

    res.send(restaurant);
});

module.exports = router;