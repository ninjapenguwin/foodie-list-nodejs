const { User } = require('../../models/user');
const request = require('supertest');

describe('/api/users', () => {
    let server;

    beforeEach(() => {
        server = require('../../../index');
    });
    afterEach(async () => {
        await User.deleteMany({});
        await server.close();
    });

    afterAll(() => {
        mongoose.connection.close();
    });

    describe('POST /', () => {
        let name;
        let email;
        let password;

        const exec = () => {
            return request(server).post('/api/users/').send({
                name,
                email,
                password,
            });
        };

        beforeEach(async () => {
            email = 'email1@gmail.com';
            (name = 'name1'), (password = 'password1');
        });

        it('should return 200 if user is registered successfully', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('it should return user object if request is valid', async () => {
            const res = await exec();

            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('email', email);
        });

        it('should return token if request is valid', async () => {
            const res = await exec();

            expect(res.header).toHaveProperty('x-auth-token');
        });

        it('it should 400 if user is already registered', async () => {
            await exec();
            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('it should return 400 if name is invalid', async () => {
            name = 'a';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('it should return 400 if email is invalid', async () => {
            email = 'a';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('it should return 400 if password is invalid', async () => {
            name = '';

            const res = await exec();

            expect(res.status).toBe(400);
        });
    });
});
